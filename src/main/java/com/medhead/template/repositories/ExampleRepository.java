package com.medhead.template.repositories;

import com.medhead.template.entities.Example;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExampleRepository extends CrudRepository<Example, Long> {
}
