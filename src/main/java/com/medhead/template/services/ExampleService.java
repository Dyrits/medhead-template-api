package com.medhead.template.services;

import com.medhead.template.entities.Example;
import com.medhead.template.repositories.ExampleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExampleService {

    private ExampleRepository repository;

    @Autowired
    public void setRepository(ExampleRepository repository){
        this.repository = repository;
    }

    public Iterable<Example> findALl() {
        return repository.findAll();
    }
}
