package com.medhead.template;

import lombok.extern.log4j.Log4j;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class MedHeadTemplateApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedHeadTemplateApiApplication.class, args);
    }

}
