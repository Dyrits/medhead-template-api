package com.medhead.template.controllers;

import com.medhead.template.entities.Example;
import com.medhead.template.services.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleController {

    private ExampleService service;

    @Autowired
    public void setService(ExampleService service) {
        this.service = service;
    }

    @GetMapping("/examples")
    public ResponseEntity<Iterable<Example>> findAll() { return ResponseEntity.ok(service.findALl()); }
}
