TITLE Docker Initializer
@ECHO off
cls
SET NAME=MedHead-Template-API
:START
ECHO.
ECHO %NAME% - Docker Initializer
ECHO 1. Initialize the network
ECHO 2. Build/Rebuild the images (BUILD)
ECHO 3. Launch the containers (UP)
ECHO 4. Relaunch the containers (DOWN/UP)
ECHO 5. Monitor the containers (LOGS)
ECHO 6. Shutdown the containers (DOWN)
ECHO 7. Exit
ECHO (By default, the app will be launch without testing and packaging. Modify the .docker-run.sh file to customize the Maven's goals.)
SET /p CHOICE=Select an option:
IF NOT '%CHOICE%'=='' SET CHOICE=%CHOICE:~0,1%
IF '%CHOICE%'=='1' GOTO INITIALIZE
IF '%CHOICE%'=='2' GOTO BUILD
IF '%CHOICE%'=='3' GOTO LAUNCH
IF '%CHOICE%'=='4' GOTO RELAUNCH
IF '%CHOICE%'=='5' GOTO LOGS
IF '%CHOICE%'=='6' GOTO SHUTDOWN
IF '%CHOICE%'=='7' GOTO EXIT
ECHO "%CHOICE%" is not valid, try again!
ECHO.
GOTO START
:INITIALIZE
ECHO Initalizing the network...
CALL docker network create medhead-network
GOTO START
:BUILD
ECHO Building the images...
CALL docker-compose build
GOTO START
:LAUNCH
ECHO Launching the containers...
CALL docker-compose up -d
GOTO START
:RELAUNCH
ECHO Shutting down the containers...
CALL docker-compose down
ECHO Relaunching the containers...
CALL docker-compose up -d
GOTO START
:LOGS
START "Logs (%NAME%)" cmd /c CALL docker-compose logs -f
GOTO START
:SHUTDOWN
ECHO Shutting down the containers...
CALL docker-compose down
GOTO START
:EXIT
PAUSE