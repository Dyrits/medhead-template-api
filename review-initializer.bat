TITLE Review Initializer
@ECHO off
cls
SET NAME=MedHead-Template-API - Review
:START
ECHO.
ECHO %NAME% - Review Initializer
ECHO Docker must be running in order to launch the tests.
ECHO 1. Launch Unit Tests
ECHO 2. Launch Integration Tests
ECHO 3. Check Code Coverage
ECHO 4. Exit
SET /p CHOICE=Select an option:
IF NOT '%CHOICE%'=='' SET CHOICE=%CHOICE:~0,1%
IF '%CHOICE%'=='1' GOTO UNIT
IF '%CHOICE%'=='2' GOTO INTEGRATION
IF '%CHOICE%'=='3' GOTO COVERAGE
IF '%CHOICE%'=='4' GOTO EXIT
ECHO "%CHOICE%" is not valid, try again!
ECHO.
GOTO START
:UNIT
ECHO Unit Tests...
CALL ./mvnw surefire-report:report
ECHO Unit Tests completed! The reports are available in the file: ./target/site/surefire-report.html and the directory ./target/surefire-reports/
GOTO START
:INTEGRATION
ECHO Integration Tests...
CALL ./mvnw clean test-compile failsafe:integration-test
ECHO Integration Tests completed! The reports are available in the directory: ./target/failsafe-reports
GOTO START
:COVERAGE
ECHO Code Coverage...
CALL ./mvnw clean org.jacoco:jacoco-maven-plugin:prepare-agent test org.jacoco:jacoco-maven-plugin:report
ECHO Code Coverage completed! The reports are available in the directory: ./target/site/jacoco
GOTO START
:EXIT
PAUSE